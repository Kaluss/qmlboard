var countTab = [0,0,0,0,0,0,0,0,0,0];
var colorIDTab = []
var tabSize = 100

function readConfigFileAndSetTheTable(listModel,boardSize,cellSize){
    var txtFile = Qt.resolvedUrl("color.json");
    var req = new XMLHttpRequest();
    var text2 = "";
    req.open("GET", txtFile, true);
    req.send(null);
    req.onreadystatechange = function(){
        if (req.readyState == 4)
        {            
            createBoard(listModel,boardSize,req.responseText);
        }
    }
}

function getColorName(colorID){
    if(colorID === 0)
    {
        return "red"
    }
    if(colorID === 1)
    {
        return "green"
    }
    if(colorID === 2)
    {
        return "blue"
    }
    if(colorID === 3 )
    {
        return "black"
    }
    if(colorID === 4)
    {
        return "yellow"
    }
    if(colorID === 5)
    {
        return "pink"
    }
    if(colorID === 6)
    {
        return "purple"
    }
    if(colorID === 7)
    {
        return "white"
    }
    if(colorID === 8)
    {
        return "brown"
    }
    if(colorID === 9)
    {
        return "gray"
    }
    console.log("Wrong color ID warning: " + myColor)
    return "-1"
}

function getColorID(myColor){
    if(myColor === "red")
    {
        return 0
    }
    if(myColor === "green")
    {
        return 1
    }
    if(myColor === "blue")
    {
        return 2
    }
    if(myColor === "black")
    {
        return 3
    }
    if(myColor === "yellow")
    {
        return 4
    }
    if(myColor === "pink")
    {
        return 5
    }
    if(myColor === "purple")
    {
        return 6
    }
    if(myColor === "white")
    {
        return 7
    }
    if(myColor === "brown")
    {
        return 8
    }
    if(myColor === "gray")
    {
        return 9
    }
    console.log("color not defined warning: " + myColor)
    return -1
   }

function setFrameColor(colorTab,thisFrameID){

    var chosenColor = "";

    while(true){
        var choice = parseInt(Math.random()*10);

        if(countTab[choice]<10){
            ++countTab[choice];
            chosenColor = colorTab[choice];            
            break;
        }
    }//while loop

    colorIDTab.push(getColorID(chosenColor))

    return chosenColor;
}

function getShowTime(){
    return (Math.random()*1000)
}

function createBoard(listModel,boardSize,config){

    var colorTab = JSON.parse(config);

    listModel.clear();


    for(var i = 0; i < boardSize; ++i) {        
        listModel.append({"colorValue": setFrameColor(colorTab),"whichID": i,"whichColorID": colorIDTab[i] ,"showDelayTime": getShowTime() ,"isFlipped": false});
    }
}

function setAllFlipped(listModel,boardSize,isFlipped){
    for(var i =0; i< boardSize; ++i){
        listModel.setProperty(i,"isFlipped", isFlipped)
    }    
}



