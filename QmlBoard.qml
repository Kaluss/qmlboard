import QtQuick 2.0
import QtQuick.Controls 1.0

import "BoardCreator.js" as BoardCreator

Rectangle {
    id: mainBoard
    width: 1000
    height: 600
    color: "lime"

    property int frameSize: 192
    property int boardSize: 100

    Timer{
        id: displayTimer
        interval: 200
        repeat: false
        onTriggered: {
            displayFrames()
        }
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            showAllFrames()
            playMyMovie(false)
        }
    }

    Component.onCompleted: {
        BoardCreator.readConfigFileAndSetTheTable(boardModel,boardSize)
        displayTimer.start()
    }

    ListModel {
        id: boardModel
    }

    ScrollView{
        anchors.fill: parent
        highlightOnFocus: true
        GridView {
            id: myGridView
            anchors.horizontalCenter: parent.horizontalCenter
            cellWidth: mainBoard.frameSize*1.5
            cellHeight: mainBoard.frameSize*1.5
            model: boardModel
            delegate: Frame {}
        }
    }

    MyMovie {
        id: myAnim
        color: parent
        anchors.centerIn: parent

    }

    function doFlipFrames(which){
       for(var i=0;i<myGridView.count;++i){
           myGridView.currentIndex = i
           myGridView.currentItem.setFlipped(which)
        }
       myGridView.currentIndex=which
    }

    function hideThisFrames(colorToHide){
        boardModel.clear();
        for(var i=0;i<100;++i){
            if(BoardCreator.colorIDTab[i] !== BoardCreator.getColorID(colorToHide))
            {
              boardModel.append({"colorValue": BoardCreator.getColorName(BoardCreator.colorIDTab[i]),"whichID": i,"whichColorID": BoardCreator.colorIDTab[i] ,"showDelayTime": BoardCreator.getShowTime() ,"isFlipped": false});
            }
         }

    }
    function showAllFrames(){
        boardModel.clear();
        for(var i=0;i<100;++i){
              boardModel.append({"colorValue": BoardCreator.getColorName(BoardCreator.colorIDTab[i]),"whichID": i,"whichColorID": BoardCreator.colorIDTab[i] ,"showDelayTime": BoardCreator.getShowTime() ,"isFlipped": false});
         }
    }

    function displayFrames(){
        for(var i=0;i<myGridView.count;++i){
            myGridView.currentIndex = i
            myGridView.currentItem.startShowTimer()
        }
        myGridView.currentIndex=0
    }

    function playMyMovie(play){
        if(play){
            //myAnim.scale = 1.0
            myAnim.playMovie()
        }
        else{
            //myAnim.scale = 0.0
            myAnim.stopMovie()
        }
    }
}

