import QtQuick 2.0
import QtMultimedia 5.0

Rectangle {
    width: 400
    height: 300
    color: "transparent"

    function playMovie(){
        player.play()
        playerLoop.loop = true
        videoOutput.scale = 1.0
    }
    function stopMovie(){
        player.stop()
        playerLoop.loop = false
        playerLoop.stop()

        videoOutput.scale = 0.0
    }

    MediaPlayer {
        id: player
        source: "filmcount.avi"
        autoPlay: false
        onStopped: {
            videoOutput.source = playerLoop
            playerLoop.play()
        }
    }

    MediaPlayer{
        id: playerLoop
        source: "Sweety.wmv"

        property bool loop: false

        onStopped: {
            if(loop){
                playerLoop.play()
            }
        }
    }

    VideoOutput {
        property bool play: false
        id: videoOutput
        source: player
        anchors.fill: parent
    }
}

