import QtQuick 2.0

import "BoardCreator.js" as BoardCreator

Flipable {
    id: singleFrame
    width: frameSize
    height: frameSize
    scale: 1.0

    property bool flipped: isFlipped
    property int frameID: whichID
    property int colorID: whichColorID
    property double showTime: showDelayTime
    property bool isMoviePlaying: false
    property bool shakeIT: false

    Timer{
        id: showTimer
        interval: singleFrame.showTime
        repeat: false
        onTriggered: {

        }
    }
    Component.onCompleted: {

    }
    function getColorID(){
        return singleFrame.colorID
    }

    function setFlipped(which){
        if(which === singleFrame.frameID){
            singleFrame.flipped = !singleFrame.flipped
        }
        else{
            singleFrame.flipped = false
        }
    }

    function startShowTimer(){
       showTimer.start()
    }

    function hideFrame(colorToHide){
        if(singleFrame.colorID === BoardCreator.getColorID(colorToHide)){
            singleFrame.scale = 0.0
        }
    }

    Timer{
        id: movieTimer
        interval: 2000
        repeat: false
        onTriggered: {
            singleFrame.isMoviePlaying = true
            mainBoard.playMyMovie(true)
        }
    }

    Timer{
        id: shakeTimer
        interval: 90
        repeat: true

        onTriggered: {
            singleFrame.shakeIT =! singleFrame.shakeIT
            if(singleFrame.shakeIT){
                singleFrame.scale = 0.8
            }
            else{
                singleFrame.scale = 1.0
            }
        }
    }

    front: Rectangle{
        id: frameFront
        enabled: !singleFrame.flipped
        anchors.fill: singleFrame
        color: colorValue

        MouseArea{
            id: mouseFront
            anchors.fill: parent
            onPressAndHold: {
               mainBoard.doFlipFrames(-1)
               console.log("Interval started")
               movieTimer.start()
               shakeTimer.start()
            }
            onReleased: {
                if(!singleFrame.isMoviePlaying){
                   mainBoard.doFlipFrames(singleFrame.frameID)
                }
                movieTimer.stop()
                shakeTimer.stop()
                singleFrame.scale = 1.0
                singleFrame.shakeIT = false
                singleFrame.isMoviePlaying = false
            }
        }
    }

    back: Rectangle{
        id: frameBack
        enabled: singleFrame.flipped
        anchors.fill: singleFrame
        color: colorValue

        MouseArea{
            id: mouseBack
            anchors.fill: parent
            onClicked: {
                mainBoard.doFlipFrames(singleFrame.frameID)
                console.log("mouseBack")
            }
        }

        //colors list
        Rectangle{
            id: colorListRec
            anchors.centerIn: parent
            width: parent.width/2
            height: parent.height/2
            color: parent.color

            ListModel{
                id: frameColorModel
                ListElement{

                    name: "red"
                }
                ListElement{
                    name: "green"
                }
                ListElement{
                    name: "blue"
                }
                ListElement{
                    name: "black"
                }
                ListElement{
                    name: "yellow"
                }
                ListElement{
                    name: "pink"
                }
                ListElement{
                    name: "purple"
                }
                ListElement{
                    name: "white"
                }
                ListElement{
                    name: "brown"
                }
                ListElement{
                    name: "gray"
                }
            }
            Component {
                id: viewDelegate
                Rectangle{
                    width: 90
                    height:11
                    color: "transparent"
                    Row {
                        id: singleRow
                        anchors.fill: parent
                        Text {
                            font.pointSize: 9
                            text: "color name: " + name
                            color: "orchid"
                        }

                    }
                    MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        mainBoard.hideThisFrames(name)
                    }

                    }
                }
            }
            ListView {
                anchors.fill: parent
                model: frameColorModel
                delegate: viewDelegate
            }
        }
    }

    //transformations
    transform: Rotation{
        id: myRotation
        origin.x: singleFrame.width/2
        origin.y: singleFrame.height/2
        axis.x: 1.5; axis.y: 0.0; axis.z: 0.0
        angle: 0
    }

    transitions: [
        Transition {
            SequentialAnimation {
                NumberAnimation{ target: singleFrame; property: "scale"; duration: 200 }
                NumberAnimation { target: myRotation; property: "angle"; easing.type: Easing.InOutBack; duration: 400 }
            }
        }
    ]

    states:[
        State{
            name: "back"
            when: singleFrame.flipped
            PropertyChanges {
                target: myRotation
                angle: 180
            }
            PropertyChanges {
                target: singleFrame
                scale: 1.5
            }
        },

        State{
            name: "front"
            when: !singleFrame.flipped
            PropertyChanges {
                target: myRotation
                angle: 0
            }
        }
    ]
}
